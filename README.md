# lwtve.xyz

some files for my website

## site

basically the whole site as it was in /var/www/html

this includes the python script used to update musicstats.html and status.html

## services

systemd services I cobbled together to run all this shit

## cfg

some configs
- nginx-default: /etc/nginx/sites-avaliable/default
- mpd.conf: /etc/mpd.conf
- icecast.xml: /etc/icecast2/icecast.xml

## important notes

- I used letsencrypt/certbot to get ssl on my nginx, your mileage may vary
- icecast must be compiled with ssl support, or use nginx proxypass for your stream
- if you have a lot of tracks (like, >10k) in your mpd library, it is strongly advised to use ashuffle to offload library shuffling to the filesystem with ashuffle; use --queue-buffer option to load up a couple of tracks into mpd, and turn on consume mode
- I also used borgbackup to back up everything, but backing up is your pain in the ass now