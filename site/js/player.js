var playPauseButton = document.getElementById("playbutton");
var muteButton = document.getElementById("mutebutton");
var slider = document.getElementById("tunesplayervolume");
var tunes = document.getElementById("tunes");
var isPlaying = false;
var isMuted = false;
var currentVolume = 1;

function playPause() {
    if (isPlaying) {
        tunes.pause();
        tunes.src = tunes.src;
        isPlaying = false;
        playPauseButton.style.animation = "";
    } else {
        tunes.play();
        isPlaying = true;
        playPauseButton.style.animation = "rainbow-filter 30s infinite";
    }
}

function changeVolume(volume) {
    tunes.volume = volume;
    currentVolume = volume;
    isMuted = false;
    muteButton.style.backgroundColor = "";
}

function toggleMute() {
    if (isMuted) {
        isMuted = false;
        tunes.volume = currentVolume;
        muteButton.style.backgroundColor = ""
    } else {
        isMuted = true;
        tunes.volume = 0;
        muteButton.style.backgroundColor = "#7a654a"
    }
}

tunes.onplaying = function() {
    playPauseButton.style.animation = "rainbow-filter 30s infinite";
    isPlaying = true;

};
tunes.onpause = function() {
    playPauseButton.style.animation = "";
    isPlaying = false;
};