var bg_count = 14;

function toggleVis() {
    vis = document.getElementById("tunesVisualizer");
    if (vis.style.visibility == "hidden") {
        vis.style.visibility = "visible";
    } else {
        vis.style.visibility = "hidden";
    }
}

function chooseBG() {
    var chosenBG = Math.floor(Math.random() * (bg_count)) + 1;
    var BGname = 'bg/bg' + chosenBG + '.gif';
    document.body.style.backgroundImage = "url('" + BGname + "')";
}

chooseBG();
loadstuff();
changetext();