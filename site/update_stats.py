#!/usr/bin/env python3

import requests
import json
from json2html import *
from contextlib import suppress
import subprocess

statstext = ''
attr = 'class="table"'
modarchive = 'https://modarchive.org/index.php?request=search&search_type=songtitle&query='

stats = requests.get('http://127.0.0.1:8000/status-json.xsl')
if (stats.status_code == 200):
    statsjson = stats.json()['icestats']
    tracklist = statsjson['source']['playlist']['trackList']
    tracks = ['<no name>', '<no name>', '<no name>', '<no name>']
    with suppress(TypeError, IndexError):
        tracks[0] = tracklist[-1]['title']
    with suppress(TypeError, IndexError):
        tracks[1] = tracklist[-2]['title']
    with suppress(TypeError, IndexError):
        tracks[2] = tracklist[-3]['title']
    with suppress(TypeError, IndexError):
        tracks[3] = tracklist[-4]['title']
    prettystats = {
        'Running since: ': statsjson['source']['stream_start'],
        'Listeners: ': statsjson['source']['listeners'],
        'Recent: ': f"{tracks[3]}, {tracks[2]}, {tracks[1]}",
        'Now playing:': tracks[0]
    }
    #statshtml = json2html.convert(json = statsjson, table_attributes = attr)
    modarchivelink = f"<a target=\"_blank\" class=\"modarchivelink\" href=\"{modarchive}{tracks[0]}\">Find track on modarchive</a>"
    statshtml = f"<div class=\"stats\" data-include=\"musicstats.html\">{json2html.convert(json = prettystats, table_attributes = attr)}\n{modarchivelink}</div>"
else:
    statshtml = '<p data-include="stats.html" class="table">No stats avaliable :(</p>'

with open('./musicstats.html', 'w') as statsfile:
    statsfile.write(statshtml)
    statsfile.close()

temp = subprocess.check_output(
    ['vcgencmd', 'measure_temp']).decode('UTF-8')[:-1]
uptime = subprocess.check_output(['uptime']).decode('UTF-8')[1:-1]
musicsize = f"{subprocess.check_output(['du', '-sh', '/home/pi/modmusic']).decode('UTF-8').split('G')[0]}GB of tracker music across {subprocess.check_output(['du', '--inodes', '/home/pi/modmusic/']).decode('UTF-8')[:5]} tracks!"

statusline = f'<p data-include="status.html" class="statustext">for nerds: {temp}   {uptime}   {musicsize}</p>'

with open('./status.html', 'w') as statusfile:
    statusfile.write(statusline)
    statusfile.close()
